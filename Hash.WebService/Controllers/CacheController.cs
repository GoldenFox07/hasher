﻿namespace Hash.WebService.Controllers
{
    using Microsoft.AspNetCore.Mvc;

    /// <summary>
    /// Контроллер работающий со списком.
    /// </summary>
    [Route("api/v1/cache")]
    [ApiController]
    public class CacheController : ControllerBase
    {
        /// <summary>
        /// Метод получения элемента по индексу.
        /// </summary>
        /// <param name="index"> Индекс элемента.</param>
        /// <returns>Возвращает элемент по индексу.</returns>
        [Route("({index})")]
        [HttpGet]
        public IActionResult GetCacheOnIndex(int index)
        {
            if (!this.ValidateIndex(index))
            {
                return this.BadRequest();
            }

            var value = Caсhe.Values[index];
            return this.Ok(value);
        }

        /// <summary>
        /// Метод добавления элемента в конец списка.
        /// </summary>
        /// <param name="numberModel"> Число которое добавляеSтся.</param>
        /// <returns>Возвращает последний элемент списка.</returns>
        [Route("")]
        [HttpPost]
        public IActionResult AddInCache([FromBody] InputNumberModel numberModel)
        {
            Caсhe.Values.Add(numberModel.Number);
            return this.Ok(Caсhe.Values[^1]);
        }

        /// <summary>
        /// Метод удаления элемента из списка по индексу.
        /// </summary>
        /// <param name="index"> индекс удаляемого элемента.</param>
        /// <returns>Возвращет сообщение об успешном удалении.</returns>
        [Route("({index})")]
        [HttpDelete]
        public IActionResult DeleteOnIndex(int index)
        {
            if (!this.ValidateIndex(index))
            {
                return this.BadRequest();
            }

            Caсhe.Values.RemoveAt(index);
            return this.Ok("Delete successed");
        }

        /// <summary>
        /// Метод замены элемента по индексу.
        /// </summary>
        /// <param name="numberModel"> новое число.</param>
        /// <param name="index"> индекс нового числа.</param>
        /// <returns>Возвращает новый элемент.</returns>
        [Route("({index})")]
        [HttpPut]
        public IActionResult UpdateOnIndex([FromBody] InputNumberModel numberModel, int index)
        {
            if (!this.ValidateIndex(index))
            {
                return this.BadRequest();
            }

            Caсhe.Values.RemoveAt(index);
            Caсhe.Values.Insert(index, numberModel.Number);
            return this.Ok(Caсhe.Values[index]);
        }

        /// <summary>
        /// Метод вывода списка.
        /// </summary>
        /// <returns>Возвращает весь список.</returns>
        [Route("")]
        [HttpGet]
        public IActionResult GetCache()
        {
            var value = Caсhe.Values;
            return this.Ok(value);
        }

        /// <summary>
        /// Метод проверки вхождения индекса в диапазон списка значений.
        /// </summary>
        private bool ValidateIndex(int index)
        {
            return (index >= 0) && (index < Caсhe.Values.Count);
        }
    }
}