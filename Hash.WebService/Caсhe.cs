﻿namespace Hash.WebService
{
    using System.Collections.Generic;

    /// <summary>
    /// Класс содержащий список чисел.
    /// </summary>
    public static class Caсhe
    {
        /// <summary>
        /// Список чисел.
        /// </summary>
        public static List<double> Values { get; set; } = new List<double>();
    }
}