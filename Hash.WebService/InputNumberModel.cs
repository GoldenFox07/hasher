﻿namespace Hash.WebService
{
    /// <summary>
    /// Класс для ввода числа из body.
    /// </summary>
    public class InputNumberModel
    {
        /// <summary>
        /// Число из body.
        /// </summary>
        public double Number { get; set; }
    }
}