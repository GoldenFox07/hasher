﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DatabaseWorker.cs" company="ООО ИК Сибинтек">
// Copyright (c) ООО ИК Сибинтек. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Hash.Service
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using Hash.Domain.Entities;
    using File = Hash.Domain.Entities.File;

    /// <summary>
    /// Класс который занимается отправкой информации о работе программы в базу данных <see cref = "DatabaseWorker"/>.
    /// </summary>
    public class DatabaseWorker
    {
        private static readonly List<Catalog> CacheCatalogs = new List<Catalog>();

        /// <summary>
        /// Метод отправляющий логи в базу данных.
        /// </summary>
        /// /// <param name="start">Время начала работы программы.</param>
        /// <param name="finish">Время конца работы программы.</param>
        /// <param name="countCatalogs">Количество каталогов всего.</param>
        /// <param name="countFiles">Количество файлов всего.</param>
        /// <param name="countSucHash">Количество файлов хэшированных успешно.</param>
        public void SaveLog(DateTime start, DateTime finish, int countCatalogs, int countFiles, int countSucHash)
        {
            using var session = SessionFactory.CreateSession();
            using (var transaction = session.BeginTransaction())
            {
                Log log = new Log()
                {
                    EndDate = finish,
                    NumberOfCatalogs = countCatalogs,
                    NumberOfFiles = countFiles,
                    StartDate = start,
                    SuccessFileHashed = countSucHash,
                    PcName = System.Environment.UserName,
                };
                session.SaveOrUpdate(log);
                transaction.Commit();
            }
        }

        /// <summary>
        /// Метод отправки информации в базу данных.
        /// </summary>
        /// <param name="path">Полный путь файла.</param>
        /// <param name="hash">Хэш сумма файла.</param>
        /// <param name="error">Описание ошибки в случае неудачи.</param>
        public void SaveFileInfo(string path, string hash, string error)
        {
            using (var session = SessionFactory.CreateSession())
            {
                using (var transaction = session.BeginTransaction())
                {
                    var catalogName = Path.GetDirectoryName(path);
                    var catalog = CacheCatalogs.FirstOrDefault(x => x.Path == Path.GetDirectoryName(path));
                    if (catalog == null)
                    {
                        catalog = session.Query<Catalog>().FirstOrDefault(x => x.Path == catalogName);
                        if (catalog == null)
                        {
                            catalog = new Catalog() { Path = catalogName, PcName = Environment.UserName, };
                            session.Save(catalog);
                        }

                        CacheCatalogs.Add(catalog);
                    }

                    var file = catalog.Files.FirstOrDefault(x => x.NameOfFile == Path.GetFileName(path));
                    if (file == null)
                    {
                        file = new File
                                        {
                                            Hash = hash,
                                            NameOfFile = Path.GetFileName(path),
                                            CatalogId = catalog.Id,
                                            Error = error,
                                        };
                        catalog.Files.Add(file);
                    }
                    else
                    {
                        file.Hash = hash;
                        file.Error = error;
                    }

                    session.SaveOrUpdate(file);
                    transaction.Commit();
                }
            }
        }
    }
}