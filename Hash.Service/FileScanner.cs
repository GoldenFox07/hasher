﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="FileScanner.cs" company="ООО ИК Сибинтек">
// Copyright (c) ООО ИК Сибинтек. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Hash.Service
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Threading;

    /// <summary>
    /// Класс который сканирует <see cref="FileScanner"/>.
    /// </summary>
    public class FileScanner
    {
        private readonly ManualResetEvent scanWaitingEvent;
        private readonly Queue<string> catalogs;

        /// <summary>
        /// Инициализирует новый экземпляр класса <see cref="FileScanner"/>.
        /// </summary>
        /// <param name="scanWaitingEvent">Событие которое говорит, что сканирование еще идет.</param>
        /// <param name="catalogs">Список файлов.</param>
        public FileScanner(ManualResetEvent scanWaitingEvent, Queue<string> catalogs)
        {
            this.scanWaitingEvent = scanWaitingEvent;
            this.catalogs = catalogs;
        }

        /// <summary>
        /// Получает или задает количесвто каталогов <see cref="CountCatalogs"/>.
        /// </summary>
        public int CountCatalogs { get; set; }

        /// <summary>
        /// Получает или задает количество файлов <see cref="CountFiles"/>.
        /// </summary>
        public int CountFiles { get; set; }

        /// <summary>
        /// Метод сканирования каталогов.
        /// </summary>
        /// <param name="path"> путь каталога.</param>
        public void ScanFiles(string path)
        {
            this.CountCatalogs++;
            try
            {
                string[] folders = Directory.GetDirectories(path);
                string[] directory = Directory.GetFiles(path);
                foreach (var folder in folders)
                {
                    this.ScanFiles(folder);
                }

                foreach (string filePath in directory)
                {
                    this.catalogs.Enqueue(filePath);
                    this.CountFiles++;
                }

                if (this.catalogs.Count > 0)
                {
                    this.scanWaitingEvent.Set();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
    }
}
