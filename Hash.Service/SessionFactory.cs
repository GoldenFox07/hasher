﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SessionFactory.cs" company="ООО ИК Сибинтек">
// Copyright (c) ООО ИК Сибинтек. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Hash.Service
{
    using System.Configuration;
    using System.Reflection;
    using FluentNHibernate.Cfg;
    using FluentNHibernate.Cfg.Db;
    using Hash.Nhibernate.Mappings;
    using NHibernate;

    /// <summary>
    /// Класс для работы с фабриками сессии.
    /// </summary>
    public static class SessionFactory
    {
        private static readonly ISessionFactory Factory;

        /// <summary>
        /// Инициализирует статические поля класса <see cref="SessionFactory"/>.
        /// </summary>
        static SessionFactory()
        {
            var configDatabase = OracleManagedDataClientConfiguration.Oracle10
                .QuerySubstitutions("true 1, false 0, yes 'Y', no 'N'");
            var fluentConfig = Fluently.Configure();
            fluentConfig.Mappings(x => x.FluentMappings.AddFromAssembly(typeof(CatalogMap).Assembly));
            var connectionString =
                "Data Source=(DESCRIPTION = (ADDRESS = (PROTOCOL = TCP)(HOST = 192.168.1.61)(PORT = 1521)) (CONNECT_DATA = (SERVER = DEDICATED)(SID = orcl))); User ID=HASHER; Password=HASHER";
            Factory = fluentConfig.Database(configDatabase.ConnectionString(connectionString)).BuildSessionFactory();
        }

        /// <summary>
        /// Возвращает сессию.
        /// </summary>
        /// <returns>Сессия.</returns>
        public static ISession CreateSession()
        {
            return Factory.OpenSession();
        }
    }
}