﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="FileHasher.cs" company="ООО ИК Сибинтек">
// Copyright (c) ООО ИК Сибинтек. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
// <summary>
//   Defines the Program type.
// </summary>

namespace Hash.Service
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Security.Cryptography;
    using System.Threading;

    /// <summary>
    /// Класс который занимается хэшированием <see cref = "FileHasher"/>.
    /// </summary>
    public class FileHasher
    {
        private static readonly object LockerReadFile = new object();
        private static readonly object LockerHashFile = new object();
        private static readonly object LockerSaveFile = new object();
        private static readonly object LockerSaveError = new object();

        private readonly ManualResetEvent scanWaitingEvent;

        private readonly ManualResetEvent scanFinishEvent;

        private readonly Queue<string> catalogs;

        private readonly DatabaseWorker worker;

        private readonly MD5 md5;

        /// <summary>
        /// Инициализирует новый экземпляр класса <see cref="FileHasher"/>.
        /// </summary>
        /// <param name="scanWaitingEvent">Событие которое говорит, что сканирование еще идет.</param>
        /// <param name="scanFinishEvent">Событие которое говорит, что сканирование завершено.</param>
        /// <param name="catalogs">Список файлов.</param>
        /// <param name="worker">Класс работающий с базой данных.</param>
        public FileHasher(
            ManualResetEvent scanWaitingEvent,
            ManualResetEvent scanFinishEvent,
            Queue<string> catalogs,
            DatabaseWorker worker)
        {
            this.scanFinishEvent = scanFinishEvent;
            this.scanWaitingEvent = scanWaitingEvent;
            this.catalogs = catalogs;
            this.worker = worker;
            this.md5 = new MD5CryptoServiceProvider();
        }

        /// <summary>
        /// Получает или задает счетчик успешно хэшированных файлов <see cref="CountSucHash"/>.
        /// </summary>
        public int CountSucHash { get; set; }

        /// <summary>
        /// Метод хэширования и вывода в консоль.
        /// </summary>
        public void FileHashing()
        {
            while (true)
            {
                ThreadLocal<string> fileName;
                lock (LockerReadFile)
                {
                    if (this.catalogs.Count == 0)
                    {
                        this.scanWaitingEvent.Reset();
                        if (WaitHandle.WaitAny(new WaitHandle[] { this.scanFinishEvent, this.scanWaitingEvent }) == 0)
                        {
                            break;
                        }
                    }

                    var fileNameResult = this.catalogs.Dequeue();
                    fileName = new ThreadLocal<string>(() => fileNameResult);
                }

                try
                {
                    ThreadLocal<string> hash;
                    lock (LockerHashFile)
                    {
                        using (var stream = File.OpenRead(fileName.Value))
                        {
                            var hashResult = BitConverter.ToString(this.md5.ComputeHash(stream))
                                .Replace("-", string.Empty).ToLower();
                            hash = new ThreadLocal<string>(() => hashResult);
                        }
                    }

                    lock (LockerSaveFile)
                    {
                        this.worker.SaveFileInfo(fileName.Value, hash.Value, null);
                    }

                    this.CountSucHash++;
                }
                catch (Exception exception)
                {
                    lock (LockerSaveError)
                    {
                        this.worker.SaveFileInfo(fileName.Value, null, exception.Message);
                    }
                }
            }
        }
    }
}
