﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CatalogMap.cs" company="ООО ИК Сибинтек">
// Copyright (c) ООО ИК Сибинтек. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Hash.Nhibernate.Mappings
{
    using FluentNHibernate.Mapping;
    using Hash.Domain.Entities;

    /// <summary>
    /// Класс производит маппинг отсканированного каталога <see cref = "CatalogMap"/>.
    /// </summary>
    public class CatalogMap : ClassMap<Catalog>
    {
        /// <summary>
        /// Инициализирует новый экземпляр класса <see cref="CatalogMap"/>.
        /// </summary>
        public CatalogMap()
        {
            this.Schema("HASHER");
            this.Table("CATALOG");
            this.Id(x => x.Id, "ID").GeneratedBy.Assigned();
            this.Map(x => x.PcName, "PC_NAME").Not.Nullable();
            this.Map(x => x.Path, "FULL_PATH").Not.Nullable();
            this.HasMany(x => x.Files).KeyColumn("CATALOG_ID");
        }
    }
}
