﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="FileMap.cs" company="ООО ИК Сибинтек">
// Copyright (c) ООО ИК Сибинтек. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Hash.Nhibernate.Mappings
{
    using FluentNHibernate.Mapping;
    using Hash.Domain.Entities;

    /// <summary>
    /// Класс производит маппинг отсканиованного файла <see cref = "FileMap"/>.
    /// </summary>
    public class FileMap : ClassMap<File>
    {
        /// <summary>
        /// Инициализирует новый экземпляр класса <see cref="FileMap"/>.
        /// </summary>
        public FileMap()
        {
            this.Schema("HASHER");
            this.Table("FILE_INFO");
            this.Id(x => x.Id, "ID").GeneratedBy.Assigned();
            this.Map(x => x.Error, "ERROR");
            this.Map(x => x.Hash, "HASH");
            this.Map(x => x.NameOfFile, "NAME").Not.Nullable();
            this.Map(x => x.CatalogId, "CATALOG_ID");
        }
    }
}
