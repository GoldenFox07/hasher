﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="LogMap.cs" company="ООО ИК Сибинтек">
// Copyright (c) ООО ИК Сибинтек. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Hash.Nhibernate.Mappings
{
    using FluentNHibernate.Mapping;
    using Hash.Domain.Entities;

    /// <summary>
    /// Класс производит маппинг Логов в базу данных <see cref = "LogMap"/>.
    /// </summary>
    public class LogMap : ClassMap<Log>
    {
        /// <summary>
        /// Инициализирует новый экземпляр класса <see cref="LogMap"/>.
        /// </summary>
        public LogMap()
        {
            this.Schema("HASHER");
            this.Table("LOG");
            this.Id(x => x.Id, "ID").GeneratedBy.Assigned();
            this.Map(x => x.EndDate, "FINISH").Not.Nullable();
            this.Map(x => x.StartDate, "\"START\"").Not.Nullable();
            this.Map(x => x.NumberOfCatalogs, "DIR_COUNT").Not.Nullable();
            this.Map(x => x.NumberOfFiles, "TOTAL_FILE_SCANNED").Not.Nullable();
            this.Map(x => x.PcName, "PC_NAME").Not.Nullable();
            this.Map(x => x.SuccessFileHashed, "SUCCESS_FILE_HASHED").Not.Nullable();
        }
    }
}
