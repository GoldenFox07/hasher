﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="BaseClass.cs" company="ООО ИК Сибинтек">
// Copyright (c) ООО ИК Сибинтек. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Hash.Domain.Entities
{
    using System;

    /// <summary>
    /// Класс представляющий уникальный идентификатор.
    /// </summary>
    public class BaseClass
    {
        /// <summary>
        /// Инициализирует новый экземпляр класса <see cref="BaseClass"/>.
        /// </summary>
        public BaseClass()
        {
            this.Id = Guid.NewGuid();
        }

        /// <summary>
        /// Получает или задает идентификатор объекта.
        /// </summary>
        public virtual Guid Id { get; set; }
    }
}
