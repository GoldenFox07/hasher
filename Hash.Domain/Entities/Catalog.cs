﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Catalog.cs" company="ООО ИК Сибинтек">
// Copyright (c) ООО ИК Сибинтек. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Hash.Domain.Entities
{
    using System.Collections.Generic;

    /// <summary>
    /// Класс представляющий отсканированный каталог <see cref = "Catalog"/>.
    /// </summary>
    public class Catalog : BaseClass
    {
        /// <summary>
        /// Инициализирует новый экземпляр класса <see cref="Catalog"/>.
        /// </summary>
        public Catalog()
        {
        }

        /// <summary>
        /// Получает или задает Полный путь каталога <see cref="Path"/>.
        /// </summary>
        public virtual string Path { get; set; }

        /// <summary>
        /// Получает или задает Имя компьютера <see cref="PcName"/>.
        /// </summary>
        public virtual string PcName { get; set; }

        /// <summary>
        /// Получает или задает Список файлов <see cref="Files"/>.
        /// </summary>
        public virtual IList<File> Files { get; set; } = new List<File>();

        /// <summary>
        /// Функция добавления файлв в каталог <see cref="AddFile"/>.
        /// </summary>
        /// <param name="file">Текущий файл.</param>
        public virtual void AddFile(File file)
        {
            this.Files.Add(file);
        }
    }
}
