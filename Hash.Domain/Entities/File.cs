﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="File.cs" company="ООО ИК Сибинтек">
// Copyright (c) ООО ИК Сибинтек. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Hash.Domain.Entities
{
    using System;

    /// <summary>
    /// Класс представляющий отсканированный файл <see cref = "File"/>.
    /// </summary>
    public class File : BaseClass
    {
        /// <summary>
        /// Инициализирует новый экземпляр класса <see cref="File"/>.
        /// </summary>
        public File()
        {
        }

        /// <summary>
        /// Получает или задает имя файла <see cref = "NameOfFile"/>.
        /// </summary>
        public virtual string NameOfFile { get; set; }

        /// <summary>
        /// Получает или задает хэш файла <see cref = "Hash"/>.
        /// </summary>
        public virtual string Hash { get; set; }

        /// <summary>
        /// Получает или задает ошибку, которая была при хэшировании (если была) <see cref = "Error"/>.
        /// </summary>
        public virtual string Error { get; set; }

        /// <summary>
        /// Получает или задает ID каталога в котором находится этот файл <see cref = "CatalogId"/>.
        /// </summary>
        public virtual Guid CatalogId { get; set; }
    }
}
