﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Log.cs" company="ООО ИК Сибинтек">
// Copyright (c) ООО ИК Сибинтек. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Hash.Domain.Entities
{
    using System;

    /// <summary>
    /// Класс который представляет логи программы <see cref = "Log"/>.
    /// </summary>
    public class Log : BaseClass
    {
        /// <summary>
        /// Инициализирует новый экземпляр класса <see cref="Log"/>.
        /// </summary>
        public Log()
        {
        }

        /// <summary>
        /// Получает или задает имя компьютера <see cref = "PcName"/>.
        /// </summary>
        public virtual string PcName { get; set; }

        /// <summary>
        /// Получает или задает Время запуска программы <see cref = "StartDate"/>.
        /// </summary>
        public virtual DateTime StartDate { get; set; }

        /// <summary>
        /// Получает или задает Время завершения программы <see cref = "EndDate"/>.
        /// </summary>
        public virtual DateTime EndDate { get; set; }

        /// <summary>
        /// Получает или задает Количество успешно отсканированных файлов <see cref = "SuccessFileHashed"/>.
        /// </summary>
        public virtual int SuccessFileHashed { get; set; }

        /// <summary>
        /// Получает или задает Общее количество файлов <see cref = "NumberOfFiles"/>.
        /// </summary>
        public virtual int NumberOfFiles { get; set; }

        /// <summary>
        /// Получает или задает Количество каталогов <see cref = "NumberOfCatalogs"/>.
        /// </summary>
        public virtual int NumberOfCatalogs { get; set; }
    }
}