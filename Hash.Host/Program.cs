﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Program.cs" company="ООО ИК Сибинтек">
// Copyright (c) ООО ИК Сибинтек. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
// <summary>
//   Defines the Program type.
// </summary>

namespace Hash.Host
{
    using System;
    using System.Collections.Generic;
    using System.Threading;
    using Hash.Service;

    /// <summary>
    /// Основной класс программы.
    /// </summary>
    public class Program
    {
        private static readonly Queue<string> Catalogs = new Queue<string>();

        private static void Main()
        {
            DatabaseWorker database = new DatabaseWorker();
            var startTime = DateTime.Now;
            Console.WriteLine("Введите путь ");
            string startPath = Console.ReadLine();
            ManualResetEvent waitingEvent = new ManualResetEvent(false);
            ManualResetEvent finishEvent = new ManualResetEvent(false);
            Thread[] threads = new Thread[3];
            FileScanner scanner = new FileScanner(waitingEvent, Catalogs);
            FileHasher hasher = new FileHasher(waitingEvent, finishEvent, Catalogs, database);
            Thread scanningthread = new Thread(() => scanner.ScanFiles(startPath));
            scanningthread.Start();
            for (int j = 0; j < 3; j++)
            {
                threads[j] = new Thread(hasher.FileHashing);
                threads[j].Start();
            }

            scanningthread.Join();
            finishEvent.Set();
            Console.WriteLine("Сканирование завершено");
            for (int j = 0; j < 3; j++)
            {
                threads[j].Join();
            }

            var finishTime = DateTime.Now;
            database.SaveLog(startTime, finishTime, scanner.CountCatalogs, scanner.CountFiles, hasher.CountSucHash);
            Console.WriteLine("Хэширование завершено, нажмите любую клавишу");
            Console.ReadKey();
        }
    }
}
